import 'package:body_mass/screen/authentication/login/LoginScreen.dart';
import 'package:body_mass/screen/splash/screens/SplashScreen.dart';
import 'package:flutter/material.dart';
import '../const/route.dart';

///screens
import '../../screen/splash/screens/InitialScreen.dart';

class AppRoute {
  Route generateRoute(RouteSettings settings) {
    switch (settings.name) {

      /// Start
      case RouteConst.routeHome:
        return MaterialPageRoute(
          builder: (_) => const InitialScreen(),
        );

      /// splash
      case RouteConst.splashScreen:
        return MaterialPageRoute(
          builder: (_) => const SplashScreen(),
        );

      /// login
      case RouteConst.logonScreen:
        return MaterialPageRoute(
          builder: (_) => const LoginScreen(),
        );
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
