import 'package:flutter/cupertino.dart';
import '../../const/route.dart';

void goToSplashScreen({
  required BuildContext context,
}) {
  Navigator.popAndPushNamed(
    context,
    RouteConst.splashScreen,
  );
}

void goToLoginScreen({
  required BuildContext context,
}) {
  Navigator.pushNamed(
    context,
    RouteConst.logonScreen,
  );
}
