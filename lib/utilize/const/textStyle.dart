///const
import '/utilize/const/color.dart';
import 'package:flutter/material.dart';

///Regular
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///17

const textSmRegularWhite17 =
    TextStyle(color: Colors.white, fontSize: 17, fontFamily: "dm-sans-regular");

const textSmRegularColor149Size17 =
    TextStyle(color: color_149, fontSize: 17, fontFamily: "dm-sans-regular");

///16
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

const textSmRegularColor58Size16 =
    TextStyle(color: color_58, fontSize: 16, fontFamily: "dm-sans-regular");
const textSmRegularColorWhiteSize16 =
    TextStyle(color: Colors.white, fontSize: 16, fontFamily: "dm-sans-regular");
const textSmRegularColor152Size16 =
    TextStyle(color: color_152, fontSize: 16, fontFamily: "dm-sans-regular");

///16
const textSmRegularColorBlackSize16 =
    TextStyle(color: Colors.black, fontSize: 16, fontFamily: "dm-sans-regular");

///Medium
///////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////
///16
const textSmMediumColor58Size16 =
    TextStyle(color: color_58, fontSize: 16, fontFamily: "dm-sans-medium");
const textSmMediumColorWhiteSize16 =
    TextStyle(color: Colors.white, fontSize: 16, fontFamily: "dm-sans-medium");

///18

const textSmMediumColor58Size18 =
    TextStyle(color: Colors.white, fontSize: 18, fontFamily: "dm-sans-medium");
const textSmColorMedium54_132_252Size_18 = TextStyle(
    color: color_54_131_252, fontSize: 18, fontFamily: "dm-sans-medium");

///Bold ///700
///////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////
const textSmBoldColor54_132_252Size_20 = TextStyle(
    color: color_54_131_252, fontSize: 20, fontFamily: "dm-sans-bold");

///18
const textSmBoldColor54_132_252Size_18 = TextStyle(
    color: color_54_131_252, fontSize: 18, fontFamily: "dm-sans-bold");

const textSmBoldColorBlackSize_18 =
    TextStyle(color: Colors.black, fontSize: 18, fontFamily: "dm-sans-bold");
