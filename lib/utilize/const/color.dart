import 'package:flutter/material.dart';

/// //// /// /// /// /// /// /// /// ///
const color_54_131_252 = Color.fromRGBO(54, 131, 252, 1);
const color_81_131_252 = Color.fromRGBO(81, 218, 255, 1);
const color_0_99_255 = Color.fromRGBO(0, 99, 255, 1);
const color_149 = Color.fromRGBO(149, 149, 149, 1.0);
const color_58 = Color.fromRGBO(58, 58, 58, 1.0);
const color_243_244_246 = Color.fromRGBO(243, 244, 246, 1);
const color_152 = Color.fromRGBO(152, 152, 152, 1);
