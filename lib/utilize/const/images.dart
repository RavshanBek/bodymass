const String userRunSvg = 'assets/images/svg/user_run.svg';
const String personHeartSvg = 'assets/images/svg/person_heart.svg';
const String messageSvg = 'assets/images/svg/message.svg';
const String keySvg = 'assets/images/svg/key.svg';
const String seePng = 'assets/images/png/see.png';
const String googlePng = 'assets/images/png/google.png';
const String logoPng = 'assets/images/png/logo.png';
