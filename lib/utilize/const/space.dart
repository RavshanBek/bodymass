import 'package:flutter/cupertino.dart';

/// space  height
/// 32
const spaceVer32 = SizedBox(
  height: 32,
);

const spaceVer34 = SizedBox(
  height: 34,
);
const spaceVer27 = SizedBox(
  height: 27,
);

const spaceVer20 = SizedBox(
  height: 20,
);
const spaceVer17 = SizedBox(
  height: 17,
);

const spaceVer5 = SizedBox(
  height: 5,
);
