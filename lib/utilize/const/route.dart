class RouteConst {
  ///first screen
  static const String routeHome = '/';

  /// main screen
  static const String mainPage = '/main-page';

  /// splash screen
  static const String splashScreen = '/splash';

  /// login screen
  static const String logonScreen = '/login';
}
