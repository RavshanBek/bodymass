import 'package:flutter/material.dart';

import '../../utilize/const/textStyle.dart';

class Button extends StatelessWidget {
  final Function onTap;
  final String text;
  final Color bcColor;
  final TextStyle textStyle;
  final double borderRadius;
  final EdgeInsets edgeInsets;

  const Button({
    Key? key,
    required this.text,
    required this.onTap,
    required this.bcColor,
    this.edgeInsets = const EdgeInsets.symmetric(horizontal: 25, vertical: 5),
    this.textStyle = textSmMediumColor58Size16,
    this.borderRadius = 20.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap(),
      child: Container(
        padding: edgeInsets,
        decoration: BoxDecoration(
            color: bcColor, borderRadius: BorderRadius.circular(borderRadius)),
        child: Text(
          text,
          style: textStyle,
        ),
      ),
    );
  }
}
