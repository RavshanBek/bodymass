import 'package:flutter/material.dart';

///const
import '/utilize/const/color.dart';
import '/utilize/const/images.dart';
import '../../../utilize/const/space.dart';
import '../../../utilize/const/textStyle.dart';

///component
import '/component/buttons/button.dart';
import '/screen/authentication/login/component/SmallDivder.dart';

///screens
import '/screen/authentication/login/component/LoginTextField.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  void onChangeEmail() {}

  void onChangePassword() {}

  /// switch case initial value
  bool isSwitched = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ///center logo
              Center(
                child: Image.asset(logoPng),
              ),

              ///form
              Column(
                children: [
                  ///email
                  LoginTextField(
                    label: "Email address",
                    onChange: onChangeEmail,
                    image: messageSvg,
                  ),
                  spaceVer17,

                  /// password
                  LoginTextField(
                    label: "Password",
                    onChange: onChangePassword,
                    image: keySvg,
                    seeImg: seePng,
                  ),
                  spaceVer17,

                  ///switch
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          const Text(
                            'Remember',
                            style: textSmRegularColor58Size16,
                          ),
                          Switch(
                            value: isSwitched,
                            onChanged: (value) {
                              setState(() {
                                isSwitched = value;
                              });
                            },
                            activeTrackColor: color_54_131_252,
                            activeColor: Colors.white,
                          ),
                        ],
                      ),
                      const InkWell(
                        child: Text(
                          'Forgot password?',
                          style: textSmRegularColor58Size16,
                        ),
                      )
                    ],
                  ),

                  spaceVer27,

                  ///submit
                  Button(
                    text: "Login",
                    onTap: () {},
                    bcColor: color_54_131_252,
                    textStyle: textSmMediumColorWhiteSize16,
                    edgeInsets:
                        const EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                  ),

                  spaceVer27,

                  ///
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SmallDivider(),
                      InkWell(
                        child: Container(
                          width: 50,
                          height: 50,
                          margin: const EdgeInsets.symmetric(horizontal: 10),
                          padding: const EdgeInsets.all(1),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.1),
                                  spreadRadius: 8,
                                  blurRadius: 6,
                                  offset: Offset(0, 1),
                                ),
                              ],
                              borderRadius: BorderRadius.circular(50)),
                          child: Image.asset(googlePng),
                        ),
                      ),
                      const SmallDivider(),
                    ],
                  )
                ],
              ),

              ///sign up
              Column(
                children: const [
                  Text(
                    'Create an account?',
                    style: textSmRegularColor152Size16,
                  ),
                  spaceVer5,
                  InkWell(
                    child: Text(
                      "Sign Up",
                      style: textSmBoldColor54_132_252Size_18,
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
