///
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

///const
import '/utilize/const/color.dart';
import '/utilize/const/textStyle.dart';

class LoginTextField extends StatelessWidget {
  final String image;
  final String label;
  final Function onChange;
  final String? seeImg;

  const LoginTextField(
      {Key? key,
      required this.image,
      required this.onChange,
      required this.label,
      this.seeImg})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      decoration: BoxDecoration(
          color: color_243_244_246, borderRadius: BorderRadius.circular(25)),
      child: Row(
        children: [
          Container(
            height: 50,
            width: 50,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: color_54_131_252,
                borderRadius: BorderRadius.circular(50)),
            child: SvgPicture.asset(image),
          ),
          Expanded(
              child: TextFormField(
                  onChanged: (value) => onChange(value),
                  decoration: InputDecoration(
                      contentPadding:
                          const EdgeInsets.only(left: 15, right: 15),
                      border: InputBorder.none,
                      labelText: label,
                      labelStyle: textSmRegularColor58Size16,
                      suffixIcon: seeImg != null
                          ? Image.asset(
                              seeImg ?? "",
                              width: 50,
                            )
                          : null)))
        ],
      ),
    );
  }
}
