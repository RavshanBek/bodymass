import 'package:body_mass/utilize/const/color.dart';
import 'package:flutter/material.dart';

class SmallDivider extends StatelessWidget {
  const SmallDivider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 5,
      decoration: BoxDecoration(
          color: color_243_244_246, borderRadius: BorderRadius.circular(10)),
    );
  }
}
