import 'package:flutter/material.dart';

///screens
import '/screen/splash/component/FotterWidget.dart';
import '/screen/splash/component/SplashSliderScreen.dart';
import '/screen/splash/component/Slider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  ///page view controller
  ///index
  int currentPage = 0;
  PageController pageController = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Stack(
          alignment: Alignment.bottomRight,
          children: [
            Positioned(
              child: PageView.builder(
                controller: pageController,
                itemCount: slideShow.length,
                onPageChanged: (val) => {
                  setState(() {
                    currentPage = val;
                  }),
                },
                itemBuilder: (context, index) {
                  return SplashScreenSliderWidget(
                    data: slideShow[index],
                    lengthSlide: slideShow.length,
                    currentPage: currentPage,
                  );
                },
              ),
            ),
            Positioned(
                child: FooterWidget(
              currentPage: currentPage,
              slide: slideShow,
            )),
          ],
        ),
      ),
    );
  }
}
