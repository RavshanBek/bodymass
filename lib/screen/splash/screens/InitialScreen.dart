import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

///local component
import '../component/MyClipper.dart';

/// const
import '/utilize/const/color.dart';
import '/utilize/const/images.dart';
import '/utilize/const/space.dart';
import '/utilize/const/textStyle.dart';
import '/utilize/function/navagation/authentication.dart';

class InitialScreen extends StatefulWidget {
  const InitialScreen({Key? key}) : super(key: key);

  @override
  _InitialScreenState createState() => _InitialScreenState();
}

class _InitialScreenState extends State<InitialScreen> {
  @override
  void initState() {
    /// check token set in local database
    Future.delayed(const Duration(milliseconds: 3000), () {
      goToSplashScreen(context: context);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Positioned(
              top: mediaQuery.height * .14,
              left: mediaQuery.width * .5 - 8,
              child: SvgPicture.asset(userRunSvg),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ClipPath(
                  clipper: MyClipper(),
                  child: Container(
                    color: color_54_131_252,
                    height: mediaQuery.height * .85,
                    width: double.infinity,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(personHeartSvg),
                          spaceVer32,
                          const Text(
                            "BMI Disease Tracker",
                            style: textSmRegularWhite17,
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
