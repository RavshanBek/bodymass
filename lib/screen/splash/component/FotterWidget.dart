import 'package:flutter/material.dart';
import '/screen/splash/component/Slider.dart';

/// const
import '../../../utilize/const/color.dart';
import '/utilize/function/navagation/authentication.dart';

/// component
import '/component/buttons/button.dart';
import 'IndicatorDot.dart';
import 'MyClipper.dart';

class FooterWidget extends StatelessWidget {
  final int currentPage;
  final List<SlideShowModel> slide;

  const FooterWidget({
    Key? key,
    required this.currentPage,
    required this.slide,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: MyFooterClipper(),
      child: Container(
        height: 200,
        width: double.infinity,
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [color_81_131_252, color_54_131_252])),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            for (int i = 0; i < slide.length; i++)
              if (currentPage != 2)
                GestureDetector(
                  onTap: () {},
                  child: IndicatorDot(
                    isActive: i == currentPage ? true : false,
                  ),
                ),
            if (currentPage == slide.length - 1)
              Button(
                  text: "Come In",
                  onTap: () => goToLoginScreen(context: context),
                  bcColor: Colors.white)
          ],
        ),
      ),
    );
  }
}
