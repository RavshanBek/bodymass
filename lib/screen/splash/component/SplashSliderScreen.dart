///////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////
///screen
import '/screen/splash/component/Slider.dart';

///////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////
// ////////////////////////////////////////////////////////////////////////////////
///const
import '/utilize/const/space.dart';
import '/utilize/const/textStyle.dart';
import 'package:flutter/material.dart';

class SplashScreenSliderWidget extends StatelessWidget {
  final SlideShowModel data;
  final int currentPage;
  final int lengthSlide;

  const SplashScreenSliderWidget({
    Key? key,
    required this.data,
    required this.currentPage,
    required this.lengthSlide,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Container(
          padding: const EdgeInsets.only(left: 27, right: 27, bottom: 130),
          height: mediaQuery.height * 1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                data.titlePurpose,
                style: textSmBoldColor54_132_252Size_20,
              ),
              Text(
                data.title,
                style: textSmBoldColorBlackSize_18,
              ),
              spaceVer34,
              Text(
                data.heading,
                style: textSmRegularColor149Size17,
              ),
              spaceVer20,
              Text(
                data.description,
                textAlign: TextAlign.center,
                style: textSmRegularColor58Size16,
              ),
            ],
          )),
    );
  }
}
