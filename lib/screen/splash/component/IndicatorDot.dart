import 'package:body_mass/utilize/const/color.dart';
import 'package:flutter/material.dart';

class IndicatorDot extends StatelessWidget {
  final bool? isActive;
  const IndicatorDot({Key? key, this.isActive = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 150),
      margin: const EdgeInsets.symmetric(horizontal: 8),
      padding: const EdgeInsets.all(2),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: isActive! ? color_0_99_255 : Colors.white,
          width: 2,
        ),
        color: Colors.white,
      ),
    );
  }
}
