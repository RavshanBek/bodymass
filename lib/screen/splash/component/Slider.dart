class SlideShowModel {
  final String title;
  final String description;
  final String titlePurpose;
  final String heading;

  SlideShowModel(
      {required this.title,
      required this.description,
      required this.heading,
      required this.titlePurpose});
}

final List<SlideShowModel> slideShow = [
  SlideShowModel(
      title: "(Please read the following)",
      description:
          "Our service is not a substitute for professional medical care. If you have or suspect that you may have health problems, please consult your doctor.",
      heading: "Our service is not a medical service",
      titlePurpose: "Warning!"),
  SlideShowModel(
      title: "(Please read the following)",
      description:
          "Our service is not a substitute for professional medical care. If you have or suspect that you may have health problems, please consult your doctor.",
      heading: "Our service is not a medical service",
      titlePurpose: "Warning!"),
  SlideShowModel(
      title: "(Please read the following)",
      description:
          "Our service is not a substitute for professional medical care. If you have or suspect that you may have health problems, please consult your doctor.",
      heading: "Our service is not a medical service",
      titlePurpose: "Warning!"),
];
