import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';

class HiveServices {
  late Box _box;

  static Future init() async {
    final getIt = GetIt.instance;
    getIt.registerSingleton<HiveServices>(HiveServices());
    await getIt<HiveServices>().create();
  }

  Future create() async {
    _box = await Hive.openBox("data");
  }

  String getTokenAccess() {
    return _box.get(_HiveKeys.accessToken, defaultValue: "");
  }

  void setTokenAccess(String token) {
    _box.put(_HiveKeys.accessToken, token);
  }

  String getTokenRefresh() {
    return _box.get(_HiveKeys.refreshToken, defaultValue: "");
  }

  void setTokenRefresh(String token) {
    _box.put(_HiveKeys.refreshToken, token);
  }
}

class _HiveKeys {
  static const String accessToken = "AccessToken";
  static const String refreshToken = "RefreshToken";
}
