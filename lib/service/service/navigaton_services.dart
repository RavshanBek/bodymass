import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class NavigationService {
  static Future init() async {
    GetIt locator = GetIt.instance;
    locator.registerLazySingleton(() => NavigationService());
  }

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo({required String routeName, dynamic arguments}) {
    return navigatorKey.currentState!
        .pushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> navigateToReplacement(
      {required String routeName, dynamic arguments}) {
    return navigatorKey.currentState!
        .pushReplacementNamed(routeName, arguments: arguments);
  }
}
