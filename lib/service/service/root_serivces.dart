import 'package:body_mass/service/service/hive_services.dart';
import 'package:get_it/get_it.dart';

import '../bloc/appBloc.dart';
import 'navigaton_services.dart';

class RootServices {
  static final _getIt = GetIt.instance;

  static Future init() async {
    if (!_getIt.isRegistered<RootServices>()) {
      _getIt.registerSingleton<RootServices>(RootServices());
      await _getIt<RootServices>().initServices();
    }
  }

  /// init services
  Future initServices() async {
    /// init hive service
    await HiveServices.init();

    /// navigation service
    await NavigationService.init();

    /// App bloc
    await AppBloc.init();
  }

  /// get like hiveService
  static HiveServices get hiveService => _getIt.get<HiveServices>();
  static NavigationService get navigationService =>
      _getIt.get<NavigationService>();
}
