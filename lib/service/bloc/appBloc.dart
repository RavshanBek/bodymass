import 'package:get_it/get_it.dart';

class AppBloc {
  static final _getIt = GetIt.instance;

  static Future init() async {
    _getIt.registerSingleton<AppBloc>(AppBloc());
    await _getIt<AppBloc>().create();
  }

  Future create() async {}
}
